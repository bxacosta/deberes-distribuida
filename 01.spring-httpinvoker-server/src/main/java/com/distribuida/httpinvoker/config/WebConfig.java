package com.distribuida.httpinvoker.config;

import com.distribuida.httpinvoker.servicios.ServicioOperaciones;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    ServicioOperaciones operaciones;

    @Bean(name = "/operaciones")
    public HttpInvokerServiceExporter httpInvokerServiceExporter() {
        HttpInvokerServiceExporter exporter = new HttpInvokerServiceExporter();

        exporter.setService(operaciones);
        exporter.setServiceInterface(ServicioOperaciones.class);

        return exporter;
    }
}
