package com.distribuida.httpinvoker.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.distribuida.httpinvoker.servicios")
public class HttpInvokerServerConfig {
}
