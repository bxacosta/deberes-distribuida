package com.distribuida.httpinvoker.controller;

import com.distribuida.httpinvoker.servicios.ServicioOperaciones;
import org.springframework.context.ApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@ManagedBean
@RequestScoped
public class IndexController implements Serializable {

    private int n1;
    private int n2;
    private int resultado;
    private ServicioOperaciones operaciones;

    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct IndexControler");

        ApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());

        operaciones = ctx.getBean(ServicioOperaciones.class);
    }

    public void sumar() {
        System.out.println("Metodo sumar()");
        resultado = operaciones.sumar(n1, n2);
    }

    // Set and Get
    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public int getN1() {
        return n1;
    }

    public void setN1(int n1) {
        this.n1 = n1;
    }

    public int getN2() {
        return n2;
    }

    public void setN2(int n2) {
        this.n2 = n2;
    }
}
