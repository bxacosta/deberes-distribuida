package com.distribuida.httpinvoker.test;

import com.distribuida.httpinvoker.config.HttpInvokerClientConfig;
import com.distribuida.httpinvoker.servicios.ServicioOperaciones;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class HttpInvokerSpringClientMain {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HttpInvokerClientConfig.class);

        ServicioOperaciones operaciones = context.getBean(ServicioOperaciones.class);

        System.out.println("Servicio: " + operaciones.getClass());

        int respuesta = operaciones.sumar(4, 3);

        System.out.println("Respuesta: " + respuesta);
    }
}
