package com.distribuida.httpinvoker.config;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

public class WebInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();

        ctx.register(HttpInvokerClientConfig.class);

        ServletRegistration.Dynamic servlet = servletContext.addServlet("javax.faces.webapp.FacesServlet", new DispatcherServlet(ctx));
        servlet.addMapping("/");

        servletContext.addListener(new ContextLoaderListener(ctx));
    }
}
