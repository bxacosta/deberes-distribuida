package com.distribuida.httpinvoker.config;

import com.distribuida.httpinvoker.servicios.ServicioOperaciones;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
public class HttpInvokerClientConfig {

    public static final String SERVICE_NAME = "http://localhost:8080/operaciones";

    @Bean
    public HttpInvokerProxyFactoryBean exporter() {
        HttpInvokerProxyFactoryBean proxy = new HttpInvokerProxyFactoryBean();

        proxy.setServiceUrl(SERVICE_NAME);
        proxy.setServiceInterface(ServicioOperaciones.class);

        return proxy;
    }
}
