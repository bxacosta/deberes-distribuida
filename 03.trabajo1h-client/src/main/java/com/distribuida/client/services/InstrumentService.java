package com.distribuida.client.services;

import com.distribuida.client.entities.Instrument;

import java.util.List;

public interface InstrumentService {

    Instrument save(Instrument instrument);
    List<Instrument> findAll();

}
