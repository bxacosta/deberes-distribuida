package com.distribuida.client.entities;

import java.io.Serializable;
import java.util.Date;

public class Album implements Serializable {


    private Integer id;

    private String title;

    private Date releaseDate;

    private int version;

    private Singer singer;
}
