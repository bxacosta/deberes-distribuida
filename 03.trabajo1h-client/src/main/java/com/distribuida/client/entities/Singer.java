package com.distribuida.client.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Singer implements Serializable {

    private Integer id;

    private String firstName;

    private String lastName;


    private Date birthDate;

    private Integer version;

    private List<Album> albums;

    public Singer() {
        albums = new ArrayList<>();
    }
}
