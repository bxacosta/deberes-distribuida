package com.distribuida.client.config;

import com.distribuida.client.services.InstrumentService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerProxyFactoryBean;

@Configuration
public class HttpInvokerClientConfig {

    public static final String SERVICE_NAME = "http://localhost:8080/instrumentService";

    @Bean
    public HttpInvokerProxyFactoryBean exporter() {
        HttpInvokerProxyFactoryBean proxy = new HttpInvokerProxyFactoryBean();

        proxy.setServiceUrl(SERVICE_NAME);
        proxy.setServiceInterface(InstrumentService.class);

        return proxy;
    }
}
