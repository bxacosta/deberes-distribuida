package com.distribuida.client.test;

import com.distribuida.client.config.HttpInvokerClientConfig;
import com.distribuida.client.entities.Instrument;
import com.distribuida.client.services.InstrumentService;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class HttpInvokerSpringClientMain {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(HttpInvokerClientConfig.class);

        InstrumentService instrumentService = context.getBean(InstrumentService.class);

        Instrument instrument = new Instrument();

        instrumentService.save(instrument);

        List<Instrument> instruments = instrumentService.findAll();

        for (Instrument i : instruments) {
            System.out.println(i);
        }
    }
}
