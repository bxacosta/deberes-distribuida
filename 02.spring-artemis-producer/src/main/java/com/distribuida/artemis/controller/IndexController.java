package com.distribuida.artemis.controller;

import com.distribuida.artemis.model.Numeros;
import com.distribuida.artemis.service.MessageSender;
import com.distribuida.artemis.service.ResultService;
import org.springframework.context.ApplicationContext;
import org.springframework.web.jsf.FacesContextUtils;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import java.io.Serializable;

@ManagedBean
@RequestScoped
public class IndexController implements Serializable {

    private MessageSender messageSender;
    private ResultService resultService;

    private String result;

    @PostConstruct
    public void init() {
        System.out.println("@PostConstruct IndexController");

        ApplicationContext ctx = FacesContextUtils.getWebApplicationContext(FacesContext.getCurrentInstance());
        messageSender = ctx.getBean(MessageSender.class);
        resultService = ctx.getBean(ResultService.class);

    }

    public void enviar(Numeros numeros) {
        messageSender.sendMessage(numeros);
    }

    public void resultado() {
        result = resultService.getResult();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
