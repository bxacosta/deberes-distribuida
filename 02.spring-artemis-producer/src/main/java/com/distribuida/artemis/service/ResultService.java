package com.distribuida.artemis.service;

import org.springframework.stereotype.Component;

@Component
public class ResultService {

    private String result;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
