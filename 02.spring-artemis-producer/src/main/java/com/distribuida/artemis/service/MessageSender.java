package com.distribuida.artemis.service;

import com.distribuida.artemis.model.Numeros;

public interface MessageSender {

    void sendMessage(String message);

    void sendMessage(Numeros numeros);

}
