package com.distribuida.artemis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

@Component("messageListener")
public class MessageListener {

    @Autowired
    private ResultService resultService;

    @JmsListener(destination = "operaciones", containerFactory = "jmsListenerContainerFactory")
    public void onMessage(Message message) throws JMSException {
        if (message instanceof TextMessage) {
            TextMessage textMessage = (TextMessage) message;

            System.out.println(">>> Text Received: " + textMessage.getText());

            resultService.setResult(textMessage.getText());
        } else if (message instanceof ObjectMessage){
            ObjectMessage objectMessage = (ObjectMessage) message;

            System.out.println(">>> Object Received: " + objectMessage.getObject().toString());
        } else {
            System.out.println("Message is not a instance of TextMessage or ObjectMessage");
        }
    }
}
