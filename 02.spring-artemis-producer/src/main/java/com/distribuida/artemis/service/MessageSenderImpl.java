package com.distribuida.artemis.service;

import com.distribuida.artemis.model.Numeros;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

import javax.jms.*;

@Component(value = "messageSender")
public class MessageSenderImpl implements MessageSender{

    @Autowired
    private JmsTemplate jmsTemplate;

    @Override
    public void sendMessage(String message) {
        jmsTemplate.setDeliveryDelay(5000L);

        this.jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                TextMessage jmsMessage = session.createTextMessage(message);
                System.out.println(">>> Sending Text: " + jmsMessage.getText());
                return jmsMessage;
            }
        });
    }

    @Override
    public void sendMessage(Numeros numeros) {
        jmsTemplate.setDeliveryDelay(5000L);

        this.jmsTemplate.send(new MessageCreator() {
            @Override
            public Message createMessage(Session session) throws JMSException {
                ObjectMessage jmsMessage = session.createObjectMessage(numeros);
                System.out.println(">>> Sending Object: " + jmsMessage.getObject().toString());
                return jmsMessage;
            }
        });
    }
}
