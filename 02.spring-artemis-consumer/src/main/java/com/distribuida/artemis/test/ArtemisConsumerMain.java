package com.distribuida.artemis.test;

import com.distribuida.artemis.config.ArtemisConfig;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.io.IOException;

public class ArtemisConsumerMain {


    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ArtemisConfig.class);
        System.out.println("Consumer Listening...");

        System.in.read();
        context.close();
    }
}
