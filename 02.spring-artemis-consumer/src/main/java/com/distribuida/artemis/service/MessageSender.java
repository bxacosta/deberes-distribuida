package com.distribuida.artemis.service;

public interface MessageSender {

    void sendMessage(String message);

}
