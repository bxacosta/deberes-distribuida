package com.distribuida.artemis.service;

import org.springframework.stereotype.Component;

@Component
public class ServicioOperacionesImpl implements ServicioOperaciones {

    @Override
    public int sumar(int n1, int n2) {
        return n1 + n2;
    }
}
