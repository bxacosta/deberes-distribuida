package com.distribuida.server.repositories;

import com.distribuida.server.entities.Album;
import org.springframework.data.repository.CrudRepository;

public interface AlbumRepository extends CrudRepository<Album, Integer> {

}
