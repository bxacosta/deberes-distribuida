package com.distribuida.server.repositories;

import com.distribuida.server.entities.Singer;
import org.springframework.data.repository.CrudRepository;

public interface SingerRepository extends CrudRepository<Singer, Integer> {
}
