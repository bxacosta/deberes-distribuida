package com.distribuida.server.repositories;

import com.distribuida.server.entities.Instrument;
import org.springframework.data.repository.CrudRepository;


public interface InstrumentRepository extends CrudRepository<Instrument, Integer> {

}
