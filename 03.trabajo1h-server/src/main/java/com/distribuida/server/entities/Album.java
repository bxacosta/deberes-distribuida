package com.distribuida.server.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "album")
public class Album implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Setter @Getter
    private Integer id;

    @Column(name = "title")
    @Getter @Setter
    private String title;

    @Temporal(TemporalType.DATE)
    @Column(name = "releaseDate")
    @Getter @Setter
    private Date releaseDate;

    @Version
    @Column(name = "version")
    @Getter @Setter
    private int version;

    @ManyToOne
    @JoinColumn(name = "singer_id")
    @Getter @Setter
    private Singer singer;
}
