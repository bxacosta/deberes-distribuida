package com.distribuida.server.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "singer")
public class Singer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Setter @Getter
    private Integer id;

    @Column(name = "first_name")
    @Getter @Setter
    private String firstName;

    @Column(name = "last_name")
    @Getter @Setter
    private String lastName;

    @Temporal(TemporalType.DATE)
    @Column(name = "birth_date")
    @Getter @Setter
    private Date birthDate;

    @Version
    @Column(name = "version")
    @Getter @Setter
    private Integer version;

    @OneToMany(mappedBy = "singer")
    private List<Album> albums;

    public Singer() {
        albums = new ArrayList<>();
    }
}
