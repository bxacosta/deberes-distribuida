package com.distribuida.server.config;

import com.distribuida.server.services.InstrumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.httpinvoker.HttpInvokerServiceExporter;

@Configuration
@ComponentScan(basePackages = "com.distribuida.server.services")
public class HttpInvokerServerConfig {


}
