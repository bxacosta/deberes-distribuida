package com.distribuida.server.services;

import com.distribuida.server.entities.Instrument;

import java.util.List;

public interface InstrumentService {

    Instrument save(Instrument instrument);
    List<Instrument> findAll();

}
