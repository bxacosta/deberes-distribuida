package com.distribuida.server.services;

import com.distribuida.server.entities.Instrument;
import com.distribuida.server.repositories.InstrumentRepository;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class InstrumentServiceImpl implements InstrumentService {

    @Autowired
    private InstrumentRepository instrumentRepository;


    @Override
    public Instrument save(Instrument instrument) {
        return instrumentRepository.save(instrument);
    }

    @Override
    public List<Instrument> findAll() {
        return Lists.newArrayList(instrumentRepository.findAll());
    }
}
